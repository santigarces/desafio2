// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyBsLr-qCIAbcvb8wJDSqBO-GUSTqo56Qrc",
    authDomain: "pelisup-1f3e6.firebaseapp.com",
    projectId: "pelisup-1f3e6",
    storageBucket: "pelisup-1f3e6.appspot.com",
    messagingSenderId: "732160314056",
    appId: "1:732160314056:web:6ecfafa264fd5e49fe33fd",
    measurementId: "G-3LTTQNFM84"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
