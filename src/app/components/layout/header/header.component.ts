import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  isLogged:boolean=false

  constructor(private _AuthService: AuthService,private router: Router) { }

  ngOnInit(): void {
   
    if( localStorage.getItem('Usuario')!=null){
      this.isLogged=true

    }else{
      this.isLogged=false
    }
  }
  salir(){
    this._AuthService.logOut()
    this.router.navigate(["/ingresar"])
  }

}
