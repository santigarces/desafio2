import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageTitleComponent } from './page-title/page-title.component';
import { FileterButtonComponent } from './fileter-button/fileter-button.component';
import { CardsComponent } from './cards/cards.component';
import { CoverImageComponent } from './cover-image/cover-image.component';
import {RouterModule} from '@angular/router';
import { DetailMovieComponent } from './detail-movie/detail-movie.component';
// import { FileterButtonComponent } from './fileter-button/fileter-button.component';



@NgModule({
  declarations: [PageTitleComponent,
    FileterButtonComponent,
    CardsComponent,
    CoverImageComponent,
    DetailMovieComponent],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports:[
    PageTitleComponent,
    FileterButtonComponent,
    CardsComponent,
    CoverImageComponent,
    DetailMovieComponent
  ]
})
export class SharedModule { }
