import { Component, Input, OnInit } from '@angular/core';
import { Movie } from 'src/app/interfaces/movie.interface';
import { MoviesService } from 'src/app/services/movies.service';

@Component({
  selector: 'app-cards',
  templateUrl: './cards.component.html',
  styleUrls: ['./cards.component.css'],
})
export class CardsComponent implements OnInit {
  @Input() filter: string | undefined;
  @Input() total: string | undefined;
  @Input() movie!: Movie;
  @Input() type!: string;
  @Input() fav!: boolean;
  @Input() Sectiofav!: boolean;
  Msg!:string
  icon!:string
  isLogged!:boolean;
  
  imageBase: string = this.movieService.imageBaseUrl;
  constructor(private movieService: MoviesService) {  
   
  }

  ngOnInit(): void {
    if( localStorage.getItem('Usuario')!=null){
      this.isLogged=true

    }else{
      this.isLogged=false
    }
    if (this.movie.fav){
    this.Msg= "Quitar de lista"
    this.icon="minus"
    }
    else
    {  
      this.Msg="Agregar a lista"
      this.icon="plus"
    }
  }
  add(){
    if (this.Msg!="Quitar de lista")
    { 
      this.Msg= "Quitar de lista"
      this.icon="minus"
      this.movieService.AddFavoritosSerie(this.movie)
    }else
    { 
      this.Msg="Agregar a lista"
      this.icon="plus"
      this.movieService.delFavoritosSerie(this.movie.id_firebase)
    }
    

  }
 
}
