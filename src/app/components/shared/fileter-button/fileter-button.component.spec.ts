import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FileterButtonComponent } from './fileter-button.component';

describe('FileterButtonComponent', () => {
  let component: FileterButtonComponent;
  let fixture: ComponentFixture<FileterButtonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FileterButtonComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FileterButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
