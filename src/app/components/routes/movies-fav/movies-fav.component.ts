import { Component, OnInit } from '@angular/core';
import { Movie } from 'src/app/interfaces/movie.interface';
import { MoviesService } from 'src/app/services/movies.service';

@Component({
  selector: 'app-movies-fav',
  templateUrl: './movies-fav.component.html',
  styleUrls: ['./movies-fav.component.css']
})
export class MoviesFavComponent implements OnInit {

  type!:string;
  value!:string;
  movies!:Movie[] 
  FilterMovies!:Movie[]
  cant!:number
  isload:boolean=false
constructor( private movieService:MoviesService) { }
  ngOnInit(): void {
    this.getMovies()
  }
  getMovies(){
    if(this.movieService.dataMovie != undefined){
      this.movies =this.movieService.dataMovie
      this.FilterMovies=this.movies
      for (let index = 0; index < this.FilterMovies.length; index++) {
        this.FilterMovies[index].fav=true
      }
      this.cant=this.FilterMovies.length
      this.isload=true
    }
    else{
      this.movieService.GetFavoritosMovie(JSON.parse(<string>localStorage.getItem('Usuario')).user.email)
      setTimeout(() => {
        this.movies =this.movieService.dataMovie
        this.FilterMovies=this.movies
        for (let index = 0; index < this.FilterMovies.length; index++) {
                this.FilterMovies[index].fav=true
          }
          this.cant=this.FilterMovies.length
          this.isload=true
        
       }, 2000);
      
    }
  }
  search(){
    this.FilterMovies = this.movies.filter(movie =>{ return movie.name.toLocaleLowerCase().match(this.value.toLocaleLowerCase())}) ;
    this.cant=this.FilterMovies.length
  }
}
