import { Component, OnInit } from '@angular/core';
import { Movie } from 'src/app/interfaces/movie.interface';
import { MoviesService } from 'src/app/services/movies.service';

@Component({
  selector: 'app-agregar',
  templateUrl: './agregar.component.html',
  styleUrls: ['./agregar.component.css']
})
export class AgregarComponent implements OnInit {
  movies!: Movie[];
  FilterMovies!: Movie[];
  isload:boolean=false
  constructor(private movieService: MoviesService) {}
  ngOnInit(): void {
    this.getTrendingAll();
  }
  getTrendingAll() {
        this.movieService.GetFavoritosMovie(JSON.parse(<string>localStorage.getItem('Usuario')).user.email)
        this.movieService.GetFavoritosSerie(JSON.parse(<string>localStorage.getItem('Usuario')).user.email)
        setTimeout(() => {
          for (let index = 0; index < this.movieService.dataMovie.length; index++) {
            for (let filerMovie = 0; filerMovie < this.FilterMovies.length; filerMovie++) {
              if (this.FilterMovies[filerMovie].title!=undefined){
                if (this.FilterMovies[filerMovie].id.toString()==this.movieService.dataMovie[index].id){
                  this.FilterMovies[filerMovie].id_firebase=this.movieService.dataMovie[index].id_firebase
                  this.FilterMovies[filerMovie].fav=true
                }
              }
            }
            }
            for (let index = 0; index < this.movieService.dataSerie.length; index++) {
              for (let filerMovie = 0; filerMovie < this.FilterMovies.length; filerMovie++) {
                if (this.FilterMovies[filerMovie].title==undefined){
                  if (this.FilterMovies[filerMovie].id.toString()==this.movieService.dataSerie[index].id){
                    this.FilterMovies[filerMovie].id_firebase=this.movieService.dataSerie[index].id_firebase.toString()
                    this.FilterMovies[filerMovie].fav=true
                  }
                }
              }
            }
            this.isload=true
         }, 2000);
      
    this.movieService.getTrending().subscribe(
      (response) => {
        this.movies = response.results;
        this.FilterMovies = this.movies.filter(
          (movie) => movie.media_type != 'person');
      },
      (error) => {
        console.error('tuve un Error' + error);
      }
    );
  
      
    }
    

}
