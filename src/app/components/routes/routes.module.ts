import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InicioComponent } from './inicio/inicio.component';
// import { RoutesRoutingModule } from './routes-routing.module';
import { SharedModule } from '../shared/shared.module';
import { PeliculasComponent } from './peliculas/peliculas.component';
import { SeriesComponent } from './series/series.component';
import { IngresarComponent } from './ingresar/ingresar.component';
import { FormsModule } from '@angular/forms';
import { LayoutModule } from '../layout/layout.module';
import { VistaMovieComponent } from './vista-movie/vista-movie.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AgregarComponent } from './agregar/agregar.component';
import { SeriesFavComponent } from './series-fav/series-fav.component';
import { MoviesFavComponent } from './movies-fav/movies-fav.component';
import {RouterModule} from '@angular/router';



@NgModule({
  declarations: [
    InicioComponent,
    
    PeliculasComponent,
    SeriesComponent,
    IngresarComponent,
    VistaMovieComponent,
    DashboardComponent,
    AgregarComponent,
    SeriesFavComponent,
    MoviesFavComponent
  ],
  imports: [
    CommonModule,
    LayoutModule,
    // RoutesRoutingModule,
    SharedModule,
    FormsModule,
    RouterModule
    
  ],
  exports:[
    InicioComponent,
    PeliculasComponent,
    SeriesComponent,
    IngresarComponent,
    VistaMovieComponent,
    DashboardComponent,
    AgregarComponent,
    SeriesFavComponent,
    MoviesFavComponent
  ]
})
export class RoutesModule { }
