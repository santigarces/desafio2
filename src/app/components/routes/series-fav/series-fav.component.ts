import { Component, OnInit } from '@angular/core';
import { Movie } from 'src/app/interfaces/movie.interface';
import { MoviesService } from 'src/app/services/movies.service';

@Component({
  selector: 'app-series-fav',
  templateUrl: './series-fav.component.html',
  styleUrls: ['./series-fav.component.css']
})
export class SeriesFavComponent implements OnInit {
  type!:string;
  value!:string;
  movies!:Movie[] 
  FilterMovies!:Movie[]
  cant!:number
  isload:boolean=false
constructor( private movieService:MoviesService) { }
  ngOnInit(): void {
    this.getMovies()
  }
  getMovies(){
    if(this.movieService.dataSerie!= undefined){
      this.movies =this.movieService.dataSerie
      this.FilterMovies=this.movies
      this.cant=this.FilterMovies.length
      for (let index = 0; index < this.FilterMovies.length; index++) {
        this.FilterMovies[index].fav=true
      }
      this.isload=true
    }
    else{
      this.movieService.GetFavoritosSerie(JSON.parse(<string>localStorage.getItem('Usuario')).user.email)
      setTimeout(() => {
        this.movies =this.movieService.dataSerie
        this.FilterMovies=this.movies
        for (let index = 0; index < this.FilterMovies.length; index++) {
                this.FilterMovies[index].fav=true
          }
          this.cant=this.FilterMovies.length
          this.isload=true
        
       }, 2000);
      
    }
  }
  search(){
    this.FilterMovies = this.movies.filter(movie =>{ return movie.name.toLocaleLowerCase().match(this.value.toLocaleLowerCase())}) ;
    this.cant=this.FilterMovies.length
  }
}
