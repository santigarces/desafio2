import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SeriesFavComponent } from './series-fav.component';

describe('SeriesFavComponent', () => {
  let component: SeriesFavComponent;
  let fixture: ComponentFixture<SeriesFavComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SeriesFavComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SeriesFavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
