import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Movie } from 'src/app/interfaces/movie.interface';
import { MoviesService } from 'src/app/services/movies.service';

@Component({
  selector: 'app-vista-movie',
  templateUrl: './vista-movie.component.html',
  styleUrls: ['./vista-movie.component.css']
})
export class VistaMovieComponent implements OnInit {
  id!:any
  type!:any
  movie!: Movie;
  vaildMovie!:boolean
  imageBase: string = this.movieService.imageBaseUrl;
  imagenPortada!:string
  imagenPoster!:string
  genero!:string
  duracionEps!:string
  firstTime:Boolean=true;
  constructor(private _route: ActivatedRoute,private movieService: MoviesService) {
    this.vaildMovie=false
   }
  ngOnInit(): void {
    this.id = this._route.snapshot.paramMap.get('id');
    this.type = this._route.snapshot.paramMap.get('type');
    if(this.type=="movie")
      this.movieDetail()
    else
    this.serieDetail()
  }
  movieDetail(){
    this.movieService.movieDetail(this.id).subscribe(
      (response) => {
        this.movie = response; 
        this.imagenPortada=this.imageBase+this.movie.backdrop_path
        this.imagenPoster=this.imageBase+this.movie.poster_path
        this.vaildMovie=true   
        
        if(this.movie.genres)
        {
          
          this.movie.genres.forEach(element => {
            if (this.firstTime){
              this.genero=element.name
              this.firstTime=false
            }
            else 
              this.genero+=', '+element.name
          })
        }
      },
      (error) => {
        console.error('tuve un Error' + error);
        this.vaildMovie=false
      }
    );
  }
  serieDetail(){
    this.movieService.serieDetail(this.id).subscribe(
      (response) => {
        this.movie = response;
        this.imagenPortada=this.imageBase+this.movie.backdrop_path
        this.imagenPoster=this.imageBase+this.movie.poster_path
        this.vaildMovie=true
        this.duracionEps=this.movie.episode_run_time[0]  
        if(this.movie.genres)
        {
          
          this.movie.genres.forEach(element => {
            if (this.firstTime){
              this.genero=element.name
              this.firstTime=false
            }
            else 
              this.genero+=', '+element.name
          })
        }
      },
      (error) => {
        console.error('tuve un Error' + error);
        this.vaildMovie=false
      }
    );

  }

}
