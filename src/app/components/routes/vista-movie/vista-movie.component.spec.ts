import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VistaMovieComponent } from './vista-movie.component';

describe('VistaMovieComponent', () => {
  let component: VistaMovieComponent;
  let fixture: ComponentFixture<VistaMovieComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VistaMovieComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VistaMovieComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
