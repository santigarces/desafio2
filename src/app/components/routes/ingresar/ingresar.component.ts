import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';

import { AuthService } from 'src/app/services/auth.service';
// import { AuthServiceService } from 'src/app/services/auth-service.service';
// import { AuthServiceService } from 'src/app/services/auth-service.service';
@Component({
  selector: 'app-ingresar',
  templateUrl: './ingresar.component.html',
  styleUrls: ['./ingresar.component.css']
})
export class IngresarComponent implements OnInit {
  ValidPass: boolean=true;
  ValidEmail!: boolean;
  firebaseErr!: boolean

  @ViewChild('miFormulario') miFormulario!: NgForm;

  initForm = {
    email: '',
    pass: "10",
    
  };
  constructor( private _AuthService: AuthService, private router: Router) { }

  ngOnInit(): void {
  
  }
  ingresar() {
    this._AuthService.login(this.miFormulario.value.email,this.miFormulario.value.pass).then(res=>{
      if(res!=null){
        localStorage.setItem('Usuario', JSON.stringify(res));
        this.firebaseErr=false
        this.router.navigate(["/dashboard"])
      }else{
        this.firebaseErr=true
      }
      
    })
  }
  newUser() {
    this._AuthService.NewUser(this.miFormulario.value.email,this.miFormulario.value.pass).then(res=>{
      if(res!=null){
        localStorage.setItem('Usuario', JSON.stringify(res));
        this.ingresar()
        // this.firebaseErr=false
      }else{
        // this.firebaseErr=true
      }
      
    })
  }
  emailvalido ():boolean{
    var EMAIL_REGEX = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/;
    if( this.miFormulario?.controls.email?.value != ""&& !this.miFormulario?.controls.email?.value.match(EMAIL_REGEX))
      this.ValidEmail=true
    else
      this.ValidEmail=false
    return (this.ValidEmail);
  }
  passvalida (){
    var PASS_REGEX = /^\S{8,256}$/;
    this.ValidPass=this.miFormulario?.controls.pass?.value != ""&& !this.miFormulario?.controls.pass?.value.match(PASS_REGEX)
   
  }


}

