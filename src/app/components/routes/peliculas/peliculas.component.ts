import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Movie } from 'src/app/interfaces/movie.interface';
import { MoviesService } from 'src/app/services/movies.service';

@Component({
  selector: 'app-peliculas',
  templateUrl: './peliculas.component.html',
  styleUrls: ['./peliculas.component.css']
})
export class PeliculasComponent implements OnInit {
 
  type!:string;
  value!:string;
  movies!:Movie[] 

FilterMovies!:Movie[]
 cant!:number
constructor( private movieService:MoviesService) { }
  ngOnInit(): void {
    this.getMovies()
  }
  getMovies(){
    this.movieService.getMovies().subscribe(response => {
    this.movies=response.results
    this.FilterMovies=this.movies.filter(movie =>  movie.media_type != 'person')
    this.cant=this.FilterMovies.length
    }, error => {
      console.error("tuve un Error" + error)
    })
    
  }


  search(){
    this.FilterMovies = this.movies.filter(movie =>{ return movie.title.toLocaleLowerCase().match(this.value.toLocaleLowerCase())}) ;
    this.cant=this.FilterMovies.length
  }
}
