import { AfterViewInit, Component, OnInit } from '@angular/core';
import { Movie } from 'src/app/interfaces/movie.interface';
import { MoviesService } from 'src/app/services/movies.service';
@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.css'],
})
export class InicioComponent implements AfterViewInit, OnInit {
  type: string = 'todos';
  tipo: string = 'todos';
  value!: string;
  cant!: number;
  movies!: Movie[];
  FilterMovies!: Movie[];
  constructor(private movieService: MoviesService) {}
  ngOnInit(): void {
    this.getTrendingAll();
    if( localStorage.getItem('Usuario')!=null){
      this.movieService.GetFavoritosMovie(JSON.parse(<string>localStorage.getItem('Usuario')).user.email)
      this.movieService.GetFavoritosSerie(JSON.parse(<string>localStorage.getItem('Usuario')).user.email)
    }
  }
  ngAfterViewInit(): void {}
  getTrendingAll() {
    this.tipo="todos"
    this.movieService.getTrending().subscribe(
      (response) => {
        this.movies = response.results;
        this.FilterMovies = this.movies.filter(
          (movie) => movie.media_type != 'person'
        );
        this.cant = this.FilterMovies.length;
      },
      (error) => {
        console.error('tuve un Error' + error);
      }
    );
  }
  getTrendingSeries(){
    this.tipo = 'series';
    this.movieService.getTrendingSeries().subscribe(
      (response) => {
        this.movies = response.results;
        this.FilterMovies = this.movies;
        this.cant = this.FilterMovies.length;
      },
      (error) => {
        console.error('tuve un Error' + error);
      }
    );
  }
  getTrendingMovies(){
    this.tipo = 'Películas';
    this.movieService.getTrendingMovies().subscribe(
      (response) => {
        this.movies = response.results;
        this.FilterMovies = this.movies;
        this.cant = this.FilterMovies.length;
      },
      (error) => {
        console.error('tuve un Error' + error);
      }
    );
}
  filter(type: string) {
    this.value = '';
    this.type = type;
    if (type == 'tv') {
      this.getTrendingSeries()
    } 
    else {
      if(this.type=="todos")
        this.getTrendingAll();
      else{
        this.getTrendingMovies()
      } 
    }
  }
  search() {
    if (this.type == 'tv') {
      this.FilterMovies = this.movies.filter((movie) => {
        return movie.name.toLocaleLowerCase().match(this.value.toLocaleLowerCase());
      });
    } else {
      if(this.type=="movie")
      {  
          this.FilterMovies = this.movies.filter((movie) => {
          return movie.title.toLocaleLowerCase().match(this.value.toLocaleLowerCase());
        });
      }else{
        this.FilterMovies = this.movies.filter((movie) => {
          if(movie.title)
           return movie.title.toLocaleLowerCase().match(this.value.toLocaleLowerCase());
          else
          return movie.name.toLocaleLowerCase().match(this.value.toLocaleLowerCase());
        });
      }
    }
    this.cant = this.FilterMovies.length;
  }
}
