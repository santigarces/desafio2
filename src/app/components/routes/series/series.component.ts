import { Component, OnInit } from '@angular/core';
import { Movie } from 'src/app/interfaces/movie.interface';
import { MoviesService } from 'src/app/services/movies.service';
@Component({
  selector: 'app-series',
  templateUrl: './series.component.html',
  styleUrls: ['./series.component.css'],
})
export class SeriesComponent implements OnInit {
  type!: string;
  value!: string;
  movies!: Movie[];
  FilterMovies!: Movie[];
  cant!: number;
  constructor(private movieService: MoviesService) {}
  ngOnInit(): void {
    this.getSeries();
  }
  getSeries() {
    this.movieService.getSeries().subscribe(
      (response) => {
        this.movies = response.results;
        this.FilterMovies = this.movies.filter(
          (movie) => movie.media_type != 'person'
        );
        this.cant = this.FilterMovies.length;
      },
      (error) => {
        console.error('tuve un Error' + error);
      }
    );
  }
  search() {
    this.FilterMovies = this.movies.filter((movie) => {
      return movie.name
        .toLocaleLowerCase()
        .match(this.value.toLocaleLowerCase());
    });
    this.cant = this.FilterMovies.length;
  }
}
