import { Component, OnInit } from '@angular/core';
import { MoviesService } from 'src/app/services/movies.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  cantMovies!:number
  cantSeries!:number
  isLoad:boolean=false

  constructor(private movieService: MoviesService) { }
  ngOnInit(): void {
    
      this.movieService.GetFavoritosMovie(JSON.parse(<string>localStorage.getItem('Usuario')).user.email)
      this.movieService.GetFavoritosSerie(JSON.parse(<string>localStorage.getItem('Usuario')).user.email)
      setTimeout(() => {
      
        this.cantMovies=this.movieService.dataMovie.length
        this.cantSeries=this.movieService.dataSerie.length
        this.isLoad=true
       }, 2000);
      
    }

  

}
