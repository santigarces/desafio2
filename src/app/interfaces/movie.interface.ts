export interface Movie {
  id: number ;
  title: string;
  name:string;
  overview?: string;
  backdrop_path:string;
  poster_path: string;
  vote_average: number;
  media_type: string;
  tagline?:string;
  release_date?:string
  runtime?:string
  genres?:any[]
  first_air_date?:string
  episode_run_time:string
  last_air_date?:string
  number_of_seasons?:number
  number_of_episodes?:string
  last_episode_to_air?:string
  next_episode_to_air?:string
  status?:string
  original_name?:string
  fav:boolean;
  id_firebase:string
  
}

