import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'desafio1';
   movies = [{
    title: "Black Widow",
    image: "/assets/portadas/img1.png",
    score:6.8
  },
  {
    title: "Shang Chi",
    image:"/assets/portadas/img2.png",
    score:7.9
  },
  {
    title: "Loki",
    image: "/assets/portadas/img3.png",
    score:8.4
  },
  {
    title: "How I Met Your Mother",
    image:"/assets/portadas/img4.png",
    score:8.3 
  },
  {
    title: "Money Heist",
    image:"/assets/portadas/img5.png",
    score:8.3 
  },
  {
    title: "Friends",
    image:"/assets/portadas/img6.png",
    score:8.8
  },
  {
    title: "The Big Bang Theory",
    image:"/assets/portadas/img7.png",
    score:8.1
  },
  {
    title: "Two And a Half Men",
    image:"/assets/portadas/img8.png",
    score:7
  },


]
}
