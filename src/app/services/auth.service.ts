import { Injectable } from '@angular/core';
import * as auth from 'firebase/auth';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import {
  AngularFirestore,
  AngularFirestoreDocument,
} from '@angular/fire/compat/firestore';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  islogged:boolean=false

  constructor(private afauth:AngularFireAuth,Firestore: AngularFirestore ) {
    
   }

  async login(email:string, password:string){
    try{
      return await this.afauth.signInWithEmailAndPassword(email,password);
    }catch(err){
      console.log("error al ingresar",err)
      return null
    }
  }
  async NewUser(email:string, password:string){
    try{
      return await this.afauth.createUserWithEmailAndPassword(email,password);
    }catch(err){
      console.log("error al crear cuenta",err)
      return null
    }
  }
  logOut(){
    localStorage.removeItem('Usuario');
  }
  
}
