import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Movie } from '../interfaces/movie.interface';
import {HttpClient} from "@angular/common/http";
import {AngularFirestore,AngularFirestoreDocument,} from '@angular/fire/compat/firestore';
@Injectable({
  providedIn: 'root'
})
export class MoviesService {
  private baseUrl :string = "https://api.themoviedb.org/3/";
  private lenguage: string="es";
  private APIKey: string="336943bdef831899be6ed3fa23e85aab";
  imageBaseUrl:string ="https://image.tmdb.org/t/p/original"
  dataMovie!:any;
  dataSerie!:any;
  constructor(private http: HttpClient,private Firestore: AngularFirestore) { 
    
  }
  getTrending():Observable<any> {
    return this.http.get<Movie[]>(`${this.baseUrl}/trending/all/week?api_key=${this.APIKey}&language=${this.lenguage}`)
    
  }
  getMovies():Observable<any> {
    return this.http.get<Movie[]>(`${this.baseUrl}movie/popular?api_key=${this.APIKey}&language=${this.lenguage}`)
    
  }
  getSeries():Observable<any> {
    return this.http.get<Movie[]>(`${this.baseUrl}tv/popular?api_key=${this.APIKey}&language=${this.lenguage}`)
    
  }
  getTrendingMovies():Observable<any> {
    return this.http.get<Movie[]>(`${this.baseUrl}trending/movie/week?api_key=${this.APIKey}&language=${this.lenguage}`)
  }
  getTrendingSeries():Observable<any> {
    return this.http.get<Movie[]>(`${this.baseUrl}trending/tv/week?api_key=${this.APIKey}&language=${this.lenguage}`)
  }
  movieDetail(id:string):Observable<any> {
    return this.http.get<Movie[]>(`${this.baseUrl}movie/${id}?api_key=${this.APIKey}&language=${this.lenguage}`)
  }
  serieDetail(id:string):Observable<any> {
    return this.http.get<Movie[]>(`${this.baseUrl}tv/${id}?api_key=${this.APIKey}&language=${this.lenguage}`) 
  }
  GetFavoritosMovie(user:any):Observable<any>{
   const test = this.Firestore.firestore.collection(`favoritos`).where("user", "==", user).where("type", "==", "movie");
    test.get().then((results:any) => { 
      this.dataMovie =results.docs.map((doc:any)=> ({
        id_firebase: doc.id,
        ...doc.data()
      }));
    })
 return this.dataMovie
}
GetFavoritosSerie(user:any):Observable<any>{
  const test = this.Firestore.firestore.collection(`favoritos`).where("user", "==", user).where("type", "==", "serie");
   test.get().then((results:any) => { 
     this.dataSerie =results.docs.map((doc:any)=> ({
      id_firebase: doc.id,
       ...doc.data()
     }));
   })
return this.dataSerie
 // const test=Firestore.firestore()
}
AddFavoritosSerie(fav:Movie){
  let type!:string
  if (fav.name==undefined){
    type= "movie"
    this.Firestore.firestore.collection(`favoritos`).add({
      name: fav.title,
      id: fav.id,
      poster_path: fav.poster_path,
      type: type,
      vote_average:fav.vote_average,
      user: JSON.parse(<string>localStorage.getItem('Usuario')).user.email

      });
  }else{
    type= "serie"
    this.Firestore.firestore.collection(`favoritos`).add({
      name: fav.name,
      id: fav.id,
      poster_path: fav.poster_path,
      type: type,
      vote_average:fav.vote_average,
      user: JSON.parse(<string>localStorage.getItem('Usuario')).user.email
      });
  }
  this.GetFavoritosSerie(JSON.parse(<string>localStorage.getItem('Usuario')).user.email)
  this.GetFavoritosMovie(JSON.parse(<string>localStorage.getItem('Usuario')).user.email)
  return
}
delFavoritosSerie(id:string){
  this.Firestore.firestore.collection(`favoritos`).doc(id).delete().then(()=>console.log("documento eliminado")).catch((err:any)=>console.log("error al eliminar"))
}
}


