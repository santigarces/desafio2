import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AgregarComponent } from './components/routes/agregar/agregar.component';
import { DashboardComponent } from './components/routes/dashboard/dashboard.component';
import { IngresarComponent } from './components/routes/ingresar/ingresar.component';
import { InicioComponent } from './components/routes/inicio/inicio.component';
import { MoviesFavComponent } from './components/routes/movies-fav/movies-fav.component';
import { PeliculasComponent } from './components/routes/peliculas/peliculas.component';
import { RoutesModule } from './components/routes/routes.module';
import { SeriesFavComponent } from './components/routes/series-fav/series-fav.component';
import { SeriesComponent } from './components/routes/series/series.component';
import { VistaMovieComponent } from './components/routes/vista-movie/vista-movie.component';


const routes: Routes = [
 
  
  {
    path: 'inicio',
    component: InicioComponent,
    pathMatch:'full'
  },
  {
    path: 'peliculas',
    component: PeliculasComponent,
    pathMatch:'full'
  },
  {
    path:'series',
    component:SeriesComponent,
    pathMatch:'full'
    
  },
  {
    path:'ingresar',
    component:IngresarComponent,
    pathMatch:'full'
    
  },
 
  {
    path:'detalle/:id/:type',
    component:VistaMovieComponent,
    pathMatch:'full'
    },
    {
      path:'dashboard',
      component:DashboardComponent,
      pathMatch:'full'
      },
      {
        path:'moviesFav',
        component:MoviesFavComponent,
        pathMatch:'full'
        },
        {
          path:'seriesFav',
          component:SeriesFavComponent,
          pathMatch:'full'
          },
      {
        path:'agregarfav',
        component:AgregarComponent,
        pathMatch:'full'
        },
    
    
     {
    path: '**',
    redirectTo:'inicio',
    pathMatch:'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
